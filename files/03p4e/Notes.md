

# Notes
- Für eine Zip zusammenstellung müssen *.csv Dateien mitgeliefert werden
- Für "basic"-Files wäre es sinnvoll *.csv Dateien mit geringer größe beizulegen:
    - damit Zip-File nicht zu groß wird
    - damit das Notebook direkt ohne weiteres durchlaufen werden kann
      
- Für "advanced"-Files sollen sich Teilnehmer größere Datensätze eigenständig runterladen (wenn es gebraucht wird -> z.B. Daten in der Auflösung 1min) 




# Probleme
 
- [ ] Übersetzung der Notebooks DE --> EN
        Starke Probleme bei autom. Übersetzung
        nbTranslate erstellt zwar "erfolgreich" ein neues Notebook mit Endung "_en", aber der Inhalt wird nicht übersetzt
        Manuelle Übersetzung mit nbTranslate (mit "Zelle übersetzen") wenig hilfreich
        ---> Bisher keine gute Alternative gefunden

- [ ] Bilder in PDF werden nicht erstellt


- [ ] 